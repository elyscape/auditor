image: cincproject/omnibus-debian
stages:
 - patch
 - package
 - test
 - cleanup
 - deploy
 - publish

variables:
  ORIGIN: https://github.com/inspec/inspec.git
  REF: master
  CHANNEL: unstable
  CINC_PRODUCT: cinc-auditor

workflow:
  rules:
    # Run if we trigger a pipeline from the web
    - if: $CI_PIPELINE_SOURCE == "web"
    # Run if this is a merge request
    - if: $CI_MERGE_REQUEST_ID

include:
  - local: .gitlab/package.yml
  - local: .gitlab/test.yml

patch:
  stage: patch
  tags:
    - docker-x86_64
  script:
    - echo "Cloning from $ORIGIN"
    - ./patch.sh
    - mkdir -p ${CI_PROJECT_DIR}/{cache,cache/git_cache}
    - echo "cache_dir '${CI_PROJECT_DIR}/cache'" >> inspec/omnibus/omnibus.rb
    - echo "git_cache_dir '${CI_PROJECT_DIR}/cache/git_cache'" >> inspec/omnibus/omnibus.rb
    - sed -i -e 's/^use_git_caching false/use_git_caching true/g' inspec/omnibus/omnibus.rb
  artifacts:
    expire_in: 1mo
    paths:
      - inspec/

# Package stage

package:amazonlinux-2:x86_64:
  extends: .package:amazonlinux
  image: cincproject/omnibus-amazonlinux:2
  cache:
    key: amazonlinux:2:x86_64
  tags:
    - docker-x86_64
  variables:
    PLATFORM_VER: "2"

package:amazonlinux-2:aarch64:
  extends: .package:amazonlinux
  image: cincproject/omnibus-amazonlinux:2
  cache:
    key: amazonlinux:2:aarch64
  tags:
    - docker-aarch64
  variables:
    PLATFORM_VER: "2"

package:centos-6:x86_64:
  extends: .package:centos
  image: cincproject/omnibus-centos:6
  cache:
    key: centos:6:x86_64
  tags:
    - docker-x86_64
  variables:
    PLATFORM_VER: "6"

package:centos-7:x86_64:
  extends: .package:centos
  image: cincproject/omnibus-centos:7
  cache:
    key: centos:7:x86_64
  tags:
    - docker-x86_64
  variables:
    PLATFORM_VER: "7"

package:centos-7:aarch64:
  extends: .package:centos
  image: cincproject/omnibus-centos:7
  cache:
    key: centos:7:aarch64
  tags:
    - docker-aarch64
  variables:
    PLATFORM_VER: "7"

package:centos-8:x86_64:
  extends: .package:centos
  image: cincproject/omnibus-centos:8
  cache:
    key: centos:8:x86_64
  tags:
    - docker-x86_64
  variables:
    PLATFORM_VER: "8"

package:centos-8:aarch64:
  extends: .package:centos
  image: cincproject/omnibus-centos:8
  cache:
    key: centos:8:aarch64
  tags:
    - docker-aarch64
  variables:
    PLATFORM_VER: "8"

package:debian-8:x86_64:
  extends: .package:debian
  image: cincproject/omnibus-debian:8
  cache:
    key: debian:8:x86_64
  tags:
    - docker-x86_64
  variables:
    PLATFORM_VER: "8"

package:debian-9:x86_64:
  extends: .package:debian
  image: cincproject/omnibus-debian:9
  cache:
    key: debian:9:x86_64
  tags:
    - docker-x86_64
  variables:
    PLATFORM_VER: "9"

package:debian-10:x86_64:
  extends: .package:debian
  image: cincproject/omnibus-debian:10
  cache:
    key: debian:10:x86_64
  tags:
    - docker-x86_64
  variables:
    PLATFORM_VER: "10"

package:macos-10.14:
  extends: .package:macos
  cache:
    key: macos:10.14
  tags:
    - macos-10.14
  variables:
    PLATFORM_VER: "10.14"

package:macos-10.15:
  extends: .package:macos
  cache:
    key: macos:10.15
  tags:
    - macos-10.15
  variables:
    PLATFORM_VER: "10.15"

package:opensuse-15:x86_64:
  extends: .package:opensuse
  image: cincproject/omnibus-opensuse:15
  cache:
    key: opensuse:15:x86_64
  tags:
    - docker-x86_64
  variables:
    PLATFORM_VER: "15"

package:windows-2012r2:
  extends: .package:windows
  tags:
    - windows-x64
  cache:
    key: windows-2012r2
  variables:
    PLATFORM_VER: "2012r2"

package:ubuntu-16.04:x86_64:
  extends: .package:ubuntu
  image: cincproject/omnibus-ubuntu:16.04
  cache:
    key: ubuntu:16.04:x86_64
  tags:
    - docker-x86_64
  variables:
    PLATFORM_VER: "16.04"

package:ubuntu-18.04:x86_64:
  extends: .package:ubuntu
  image: cincproject/omnibus-ubuntu:18.04
  cache:
    key: ubuntu:18.04:x86_64
  tags:
    - docker-x86_64
  variables:
    PLATFORM_VER: "18.04"

package:ubuntu-20.04:x86_64:
  extends: .package:ubuntu
  image: cincproject/omnibus-ubuntu:20.04
  cache:
    key: ubuntu:20.04:x86_64
  tags:
    - docker-x86_64
  variables:
    PLATFORM_VER: "20.04"

# Test stage

test:amazonlinux-2:x86_64:
  extends: .test:amazonlinux
  image: cincproject/omnibus-amazonlinux:2
  dependencies:
    - package:amazonlinux-2:x86_64
  tags:
    - docker-x86_64
  variables:
    PLATFORM_VER: "2"

test:amazonlinux-2:aarch64:
  extends: .test:amazonlinux
  image: cincproject/omnibus-amazonlinux:2
  dependencies:
    - package:amazonlinux-2:aarch64
  tags:
    - docker-aarch64
  variables:
    PLATFORM_VER: "2"

test:centos-6:x86_64:
  extends: .test:centos
  image: cincproject/omnibus-centos:6
  dependencies:
    - package:centos-6:x86_64
  tags:
    - docker-x86_64
  variables:
    PLATFORM_VER: "6"

test:centos-7:x86_64:
  extends: .test:centos
  image: cincproject/omnibus-centos:7
  dependencies:
    - package:centos-7:x86_64
  tags:
    - docker-x86_64
  variables:
    PLATFORM_VER: "7"

test:centos-7:aarch64:
  extends: .test:centos
  image: cincproject/omnibus-centos:7
  dependencies:
    - package:centos-7:aarch64
  tags:
    - docker-aarch64
  variables:
    PLATFORM_VER: "7"

test:centos-8:x86_64:
  extends: .test:centos
  image: cincproject/omnibus-centos:8
  dependencies:
    - package:centos-8:x86_64
  tags:
    - docker-x86_64
  variables:
    PLATFORM_VER: "8"

test:centos-8:aarch64:
  extends: .test:centos
  image: cincproject/omnibus-centos:8
  dependencies:
    - package:centos-8:aarch64
  tags:
    - docker-aarch64
  variables:
    PLATFORM_VER: "8"

test:debian-8:x86_64:
  extends: .test:debian
  image: cincproject/omnibus-debian:8
  dependencies:
    - package:debian-8:x86_64
  tags:
    - docker-x86_64
  variables:
    PLATFORM_VER: "8"

test:debian-9:x86_64:
  extends: .test:debian
  image: cincproject/omnibus-debian:9
  dependencies:
    - package:debian-9:x86_64
  tags:
    - docker-x86_64
  variables:
    PLATFORM_VER: "9"

test:debian-10:x86_64:
  extends: .test:debian
  image: cincproject/omnibus-debian:10
  dependencies:
    - package:debian-10:x86_64
  tags:
    - docker-x86_64
  variables:
    PLATFORM_VER: "10"

test:macos-10.14:
  extends: .test:macos
  tags:
    - macos-10.14
  dependencies:
    - package:macos-10.14
  variables:
    PLATFORM_VER: "10.14"

test:macos-10.15:
  extends: .test:macos
  tags:
    - macos-10.15
  dependencies:
    - package:macos-10.15
  variables:
    PLATFORM_VER: "10.15"

test:opensuse-15:x86_64:
  extends: .test:opensuse
  image: cincproject/omnibus-opensuse:15
  dependencies:
    - package:opensuse-15:x86_64
  tags:
    - docker-x86_64
  variables:
    PLATFORM_VER: "15"

test:ubuntu-16.04:x86_64:
  extends: .test:ubuntu
  image: cincproject/omnibus-ubuntu:16.04
  dependencies:
    - package:ubuntu-16.04:x86_64
  tags:
    - docker-x86_64
  variables:
    PLATFORM_VER: "16.04"

test:ubuntu-18.04:x86_64:
  extends: .test:ubuntu
  image: cincproject/omnibus-ubuntu:18.04
  dependencies:
    - package:ubuntu-18.04:x86_64
  tags:
    - docker-x86_64
  variables:
    PLATFORM_VER: "18.04"

test:ubuntu-20.04:x86_64:
  extends: .test:ubuntu
  image: cincproject/omnibus-ubuntu:20.04
  dependencies:
    - package:ubuntu-20.04:x86_64
  tags:
    - docker-x86_64
  variables:
    PLATFORM_VER: "20.04"

test:windows-2012r2:
  extends: .test:windows
  dependencies:
    - package:windows-2012r2
  variables:
    PLATFORM_VER: "2012r2"

.ssh-setup:
  before_script:
    - eval $(ssh-agent -s)
    - echo "${SSH_PRIVATE_KEY}" | tr -d '\r' | ssh-add - > /dev/null
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - echo "${SSH_KNOWN_HOSTS}" > ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts

deploy:
  allow_failure: false
  stage: deploy
  extends: .ssh-setup
  # Only run if this is triggered from the web
  rules:
    - if: $CI_PIPELINE_SOURCE == "web"
      when: manual
    - when: never
  tags:
    - docker-x86_64
  dependencies:
    - package:amazonlinux-2:x86_64
    - package:amazonlinux-2:aarch64
    - package:centos-6:x86_64
    - package:centos-7:x86_64
    - package:centos-7:aarch64
    - package:centos-8:x86_64
    - package:centos-8:aarch64
    - package:debian-8:x86_64
    - package:debian-9:x86_64
    - package:debian-10:x86_64
    - package:macos-10.14
    - package:macos-10.15
    - package:opensuse-15:x86_64
    - package:ubuntu-16.04:x86_64
    - package:ubuntu-18.04:x86_64
    - package:ubuntu-20.04:x86_64
    - package:windows-2012r2
  script:
    - ssh cinc@${DOWNLOADS_HOST} "mkdir -p /data/incoming/files/${CHANNEL}/cinc-auditor/$(cat VERSION)/"
    - rsync -avH --delete data/ cinc@${DOWNLOADS_HOST}:/data/incoming/files/${CHANNEL}/cinc-auditor/$(cat VERSION)/
    - ssh cinc@${DOWNLOADS_HOST} "chmod 755 /data/incoming/files/${CHANNEL}/cinc-auditor/$(cat VERSION)/"

deploy-gems:
  stage: deploy
  # Only run if this is triggered from the web
  rules:
    - if: $CI_PIPELINE_SOURCE == "web"
      when: manual
    - when: never
  tags:
    - docker-x86_64
  dependencies:
    - patch
  script:
    - ./deploy-gems.sh

publish:
  stage: publish
  extends: .ssh-setup
  dependencies: []
  # Only run if this is triggered from the web
  rules:
    - if: $CI_PIPELINE_SOURCE == "web"
      when: on_success
    - when: never
  tags:
    - downloads
  script:
    - sudo mkdir -p /data/mirror/files/${CHANNEL}/cinc-auditor
    - sudo /usr/bin/rsync -avH /data/incoming/files/${CHANNEL}/cinc-auditor/ /data/mirror/files/${CHANNEL}/cinc-auditor/
    - sudo -E -u cinc /usr/local/bin/update-cinc-api
    - ssh -q cinc@${MIRROR_HOST} "~/sync-from-master"

publish-docker:
  image: docker:latest
  services:
  - docker:dind
  stage: publish
  # Only run if this is triggered from the web
  rules:
    - if: $CI_PIPELINE_SOURCE == "web"
      when: on_success
    - when: never
  tags:
    - docker-x86_64
  dependencies:
    - patch
  script:
    - ./docker.sh
  variables:
    DOCKER_TLS_CERTDIR: "/certs"

.cleanup:
  stage: cleanup
  dependencies: []
  variables:
    GIT_CHECKOUT: "false"
  when: always
  script:
    - sudo rm -rf inspec/

cleanup:macos-10.14:
  extends: .cleanup
  tags:
    - macos-10.14

cleanup:macos-10.15:
  extends: .cleanup
  tags:
    - macos-10.15
